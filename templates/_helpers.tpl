{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "mayan.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "mayan.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "mayan.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Common labels
*/}}
{{- define "mayan.labels" -}}
helm.sh/chart: {{ include "mayan.chart" . }}
{{ include "mayan.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end -}}

{{/*
Selector labels
*/}}
{{- define "mayan.selectorLabels" -}}
app.kubernetes.io/name: {{ include "mayan.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end -}}

{{/*
Create the name of the service account to use
*/}}
{{- define "mayan.serviceAccountName" -}}
{{- if .Values.serviceAccount.create -}}
    {{ default (include "mayan.fullname" .) .Values.serviceAccount.name }}
{{- else -}}
    {{ default "default" .Values.serviceAccount.name }}
{{- end -}}
{{- end -}}


{{/*
Create a default fully qualified app name for the postgres requirement.
*/}}
{{- define "mayan.postgresql.pgpool" -}}
{{- $postgresContext := dict "Values" (index .Values "postgresql-ha") "Release" .Release "Chart" (dict "Name" "postgresql-ha") -}}
{{ include "postgresql-ha.pgpool" $postgresContext }}
{{- end }}

{{/*
Create a default fully qualified app name for the redis requirement.
*/}}
{{- define "mayan.redis.fullname" -}}
{{- $redisContext := dict "Values" .Values.redis "Release" .Release "Chart" (dict "Name" "redis") -}}
{{ printf "%s-master" (include "redis.fullname" $redisContext) }}
{{- end }}

{{/*
Mayan needs the information to wait for Postgresql and Redis before launching.
*/}}
{{- define "mayan.dockerWait" -}}
{{ printf "%s:%s %s:%s" (include "mayan.postgresql.pgpool" .) ("5432") ("mayan-redis-master") ("6379")}}
{{- end }}

{{/*
Mayan needs the celery broker url passed as string, including passwords :(
*/}}
{{- define "mayan.mayanCeleryBrokerUrl" -}}
{{ printf "redis://:%s@%s:%s/0" .Values.global.redis.password ("mayan-redis-master") ("6379") }}
{{- end }}

{{/*
Mayan needs the celery result backend url passed as string, including passwords :(
*/}}
{{- define "mayan.mayanCeleryResultBackend" -}}
{{ printf "redis://:%s@%s:%s/1" .Values.global.redis.password ("mayan-redis-master") ("6379") }}
{{- end }}