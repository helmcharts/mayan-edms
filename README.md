# Mayan-EDMS

[Mayan EDMS](https://www.mayan-edms.com/) is an open source Document Management System.

## Prerequisites

- Kubernetes 1.12+
- Helm 3.0-beta3+
- PV provisioner support in the underlying infrastructure

## Installing the Chart

Add helm repository
```
$ helm repo add helmcharts https://helmcharts.gitlab.io/stable
```

To install the chart with the release name `mayan`:

```bash
$ kubectl create namespace mayan
$ helm install mayan helmcharts/mayan --namespace=mayan
```

The command deploys Mayan-EDMS on the Kubernetes cluster in the default configuration using PostgeSQL-HA and Redis.

> **Tip**: List all releases using `helm list`

On first run the database 'mayan' needs to get created manually as soon as the postgresql master is up and reachable:

```
$ kubectl run postgresql-postgresql-client --rm --tty -i --restart='Never' --namespace mayan --image bitnami/postgresql --env='PGPASSWORD=changeMe!' --command -- psql --host mayan-postgresql-ha-pgpool -U postgres

$ CREATE DATABASE mayan;
```

## Notes
* Mayan-EDMS is not able to use Redis-Cluster nor to query Redis Sentinel for master instance, in case of an redis-master node outage the cluster we use needs to be recovered manually. See https://redis.io/commands/slaveof for details. Suggested solution here is to try using RabbitMQ as replacement for Redis. More heavy but can compensate for the deficits of mayan.
